import 'package:flutter/material.dart';

class Flag extends StatefulWidget {
  const Flag({super.key});

  @override
  State<Flag> createState() => _Flag();
}

class _Flag extends State<Flag> {
  int? count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: const Text("INDIAN FLAG"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            count = count! + 1;
          });
        },
        child: const Text("ADD"),
      ),
      body: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("$count"),
          (count! >= 1)
              ? Container(
                  width: 10,
                  height: 600,
                  color: Colors.black,
                )
              : Container(),
          Container(
            child: Column(
              children: [
                (count! >= 2)
                    ? Container(
                        width: 300,
                        height: 75,
                        color: Colors.orange,
                      )
                    : Container(),
                (count! >= 3)
                    ? Container(
                        width: 300,
                        height: 75,
                        color: Colors.white,
                        child: Column(
                          children: [
                            (count! >= 4)
                                ? Image.network(
                                    "https://t3.ftcdn.net/jpg/03/11/13/46/360_F_311134651_RXMvbUB3h089Js0ODvuHrttmsON9Tpik.jpg",
                                    height: 75,
                                  )
                                : Container(),
                          ],
                        ),
                      )
                    : Container(),
                (count! >= 5)
                    ? Container(
                        width: 300,
                        height: 75,
                        color: Colors.green,
                      )
                    : Container(),
              ],
            ),
          )
        ],
      )),
    );
  }
}

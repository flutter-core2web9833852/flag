import 'package:flutter/material.dart';
import 'package:indian_flag_v2/Flag.dart';
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Flag(),
    );
  }
}
